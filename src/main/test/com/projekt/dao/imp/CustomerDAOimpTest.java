package com.projekt.dao.imp;

import com.projekt.dao.CustomerDAO;
import com.projekt.model.Customer;
import com.projekt.model.Login;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CustomerDAOimpTest {

    @Test
    public void findbyemail() {
        CustomerDAO dao = new CustomerDAOimp();
        dao.save(new Customer("a","s","testMail@test.com","c","f","v","p"));
        Login l = new Login();
        l.setEmail("testMail@test.com");
        Customer result = dao.findbyemail(l);
        assertNotNull(result);
    }
}