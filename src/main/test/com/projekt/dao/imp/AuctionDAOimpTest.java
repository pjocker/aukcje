package com.projekt.dao.imp;

import com.projekt.model.Auction;
import com.projekt.model.Customer;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class AuctionDAOimpTest {

    @Test
    public void findFinished() {
        Auction a = new Auction(null,null ,"Przedmiot",123.0,"TestAuctionFinished","afawef");
        a.setEndDate(new Date());
        AuctionDAOimp dao = new AuctionDAOimp();
        dao.save(a);

        boolean passed = false;
        for(Auction au:dao.findFinished()){
            if(au.getTitle().equals("TestAuctionFinished")){
                passed = true;
                break;
            }
        }
        assertEquals(true,passed);
    }

    @Test
    public void findActive() {
        Auction a = new Auction(null,null ,"Przedmiot",123.0,"TestAuctionActive","afawef");
        AuctionDAOimp dao = new AuctionDAOimp();
        dao.save(a);
        boolean passed = false;
        for(Auction au : dao.findActive()){
            if(au.getTitle().equals("TestAuctionActive")){
                passed = true;
                break;
            }
        }
        assertEquals(true,passed);
    }
}