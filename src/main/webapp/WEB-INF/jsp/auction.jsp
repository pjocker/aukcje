
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>Produkty</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Aukcja</h1>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-5">
            <p>
                <strong>Kod aukcji: </strong><span class="label label-warning">${auction.id}</span>
            </p>
            <p><strong>Sprzedający: </strong>${auction.seller.firstName} ${auction.seller.lastName}</p>
            <h3>${auction.title}</h3>
            <p>
                <strong>Produkt</strong>: ${auction.productName}
            </p>
            <p>${auction.description}</p>
            <p>
                <strong>Koniec aukcji</strong>: ${auction.endDate.toString()}
            </p>
            <p>
                <strong>Kupujący </strong>: ${auction.buyer.firstName} ${auction.buyer.lastName}
            </p>
            <h4>${auction.price}PLN</h4>
            <form:form modelAttribute="bidPrice" class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-lg-2" for="price">Podbij o:</label>
                    <div clas="col-lg-10">
                        <div class="form:input-prepend">
                            <form:input id="price" path="price" type="text" class="form:input-large"/>
                            <form:errors path="price"/>
                        </div>
                    </div>
                </div>
                <form:form modelAttribute="bidlogin"  class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-lg-2 " for="email">Email</label>
                        <div class="col-lg-10">
                            <form:input path="email" id="email" type="email" rows="2"/>
                            <form:errors path="email"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2 col-lg-2" for="password">Hasło</label>
                        <div class="form:input-prepend">
                            <div class="col-lg-10">
                                <form:input id="password" path="password" type="password" class="form:input-large"/>
                                <form:errors path="password"/>
                            </div>
                        </div>
                    </div>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <input type="submit" id="btnAdd" class="btn btn-primary" value="Podbij cene"/>
                    </div>
                </div>
                </form:form>
            </form:form>
                <p>
                <a href="<spring:url value="/auctions"/>" class="btn btndefault">
                    <span class="glyphicon-hand-left glyphicon"></span>Wstecz
                </a>
            </p>
        </div>
    </div>
</section>
</body>
</html>
