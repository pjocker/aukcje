<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>Aukcje</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Aukcje</h1>
            <p>Dodaj aukcję</p>
        </div>
    </div>
</section>
<section class="container">
            <form:form modelAttribute="loginRemove"  class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-lg-2 " for="email">Email</label>
                    <div class="col-lg-10">
                        <form:input path="email" id="email" type="email" rows="1"/>
                        <form:errors path="email"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 col-lg-2" for="password">Hasło</label>
                    <div class="form:input-prepend">
                        <div class="col-lg-10">
                            <form:input id="password" path="password" type="password" class="form:input-large"/>
                            <form:errors path="password"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <input type="submit" id="btnAdd" class="btn btn-primary" value="Dodaj"/>
                    </div>
                </div>
            </form:form>
    <p>
        <a href="<spring:url value="/auctions"/>" class="btn btndefault">
            <span class="glyphicon-hand-left glyphicon"></span>Wstecz
        </a>
    </p>
</section>
</body>
</html>