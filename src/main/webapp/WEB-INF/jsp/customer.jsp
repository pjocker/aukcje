<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>Użytkownik</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Użytkownik</h1>
        </div>
    </div>
</section>
<section>

        <div class="container">
            <h1> ${customer.firstName} </h1>
            <p> ${customer.lastName} </p>
        </div>

</section>
</body>
</html>