
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>Aukcje</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Aukcje</h1>
            <p>Dodaj aukcję</p>
        </div>
    </div>
</section>
<section class="container">
    <form:form modelAttribute="customer"  class="form-horizontal">
        <fieldset>
            <legend>Dodaj nową aukcję</legend>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="firstName">Imie</label>
                <div class="col-lg-10">
                    <form:input id="firstName" path="firstName" type="text" class="form:input-large"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="lastName">Nazwisko</label>
                <div class="col-lg-10">
                    <form:input id="productName" path="lastName" type="text" class="form:input-large"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 " for="email">Email</label>
                <div class="col-lg-10">
                    <form:input path="email" id="email" type="email" rows="2" class="form:input-large"/></div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="password">Hasło</label>
                <div class="form:input-prepend">
                    <div class="col-lg-10">
                        <form:input id="password" path="password" type="password" class="form:input-large"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="address">Adres</label>
                <div class="form:input-prepend">
                    <div class="col-lg-10">
                        <form:input id="address" path="address" type="text" class="form:input-large"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="city">Miasto</label>
                <div class="form:input-prepend">
                    <div class="col-lg-10">
                        <form:input id="city" path="city" type="text" class="form:input-large"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-2 col-lg-2" for="phone">Numer</label>
                <div class="form:input-prepend">
                    <div class="col-lg-10">
                        <form:input id="phone" path="phone" type="number" class="form:input-large"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" id="btnAdd" class="btn btn-primary" value="Zarejestruj się"/>
                </div>
            </div>
        </fieldset>
    </form:form>
</section>
</body>
</html>