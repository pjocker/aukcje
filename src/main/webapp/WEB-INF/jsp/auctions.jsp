
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>Aukcje</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Aukcje</h1>
            <p>Aktualnie dostepne aukcje</p>
            <p>
            <a href="<spring:url value="/auctions/add"/>" class="btn btn-info">
                Nowa aukcja
            </a>
                <a href="<spring:url value="/auctions/remove/login"/>" class="btn btn-info">
                    Usuń swoją aukcję
                </a>
            </p>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <c:forEach items="${auctions}" var="auction">
            <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>${auction.title}</h3>
                        <p>${auction.description}</p>
                        <p>${auction.price}PLN</p>
                        <p>
                            <a href="<spring:url value="/auctions/auction?id=${auction.id}"/>"
                               class="btn btn-primary">
                                <span class="glyphicon-info-sign glyphicon"></span> Szczegóły
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
    <p>
        <a href="<spring:url value="/"/>" class="btn btndefault">
            <span class="glyphicon-hand-left glyphicon"></span>Wstecz
        </a>
    </p>
</section>
</body>
</html>