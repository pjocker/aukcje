<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>Aukcje</title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Aukcje</h1>
            <p>Twoje aukcje</p>
            <form:form modelAttribute="auction"  class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-lg-2" for="id">ID:</label>
                <div clas="col-lg-10">
                    <div class="form:input-prepend">
                        <form:input id="id" path="id" type="number" class="form:input-large"/>
                    </div>
                </div>
            </div>
                <form:form modelAttribute="login"  class="form-horizontal">
                        <td>
                            <form:label path="email">Email: </form:label>
                        </td>
                        <td>
                            <form:input path="email" name="email" id="email" type="email" />
                        </td>
                    <tr>
                        <td>
                            <form:label path="password">Password:</form:label>
                        </td>
                        <td>
                            <form:password path="password" name="password" id="password" />
                        </td>
                    </tr>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" id="btnAdd" class="btn btn-warning" value="Usuń"/>
                </div>
            </div>
            </form:form>
            </form:form>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <c:forEach items="${removeAuctions}" var="auction">

            <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
                <div class="thumbnail">
                    <div class="caption">
                        <p>${auction.id}</p>
                        <h3>${auction.title}</h3>
                        <p>${auction.description}</p>
                        <p>${auction.price}PLN</p>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
    <p>
        <a href="<spring:url value="/auctions"/>" class="btn btndefault">
            <span class="glyphicon-hand-left glyphicon"></span>Wstecz
        </a>
    </p>
</section>
</body>
</html>
