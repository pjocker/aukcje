package com.projekt.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;
@NamedQueries({
        @NamedQuery(
                name = "findSellerAuctions",
                query = "from Auction s where s.seller = :to"
        )
})
@Entity
@Table(name = "Auction")
public class Auction extends Abstract {

    @JoinColumn(name = "buyer")
    @ManyToOne
    private Customer buyer;

    @JoinColumn(name = "seller")
    @ManyToOne
    private Customer seller;

    @Column
    @org.hibernate.validator.constraints.NotBlank(message = "Należy podać nazwę produktu.")
    private String productName;

    @Column
    @Min(value = 1, message = "Minimalna cena to 1 PLN.")
    @Digits(integer = 10, fraction = 2, message = "Nieprawidłowa cena.")
    private Double price;

    @Column
    @org.hibernate.validator.constraints.NotBlank(message = "Należy podać tytuł aukcji.")
    private String title;

    @Column
    @org.hibernate.validator.constraints.NotBlank(message = "Należy podać opis aukcji.")
    private String description;

    @Column
    private Date endDate;

    @Column
    private Integer finished;

    public Auction() {
        super();

        this.price = new Double(0);
        //this.productName = new String("");
        this.finished = 0;
        this.productName = "";
    }

    public Auction(Customer buyer, Customer seller, String product, Double price, String title, String description) {
        this.buyer = buyer;
        this.seller = seller;
        this.productName = product;
        this.price = price;
        this.title = title;
        this.description = description;
        this.finished = 0;
    }

    public Auction(Customer seller, String productName, Double price, String title, String description) {
        this.seller = seller;
        this.productName = productName;
        this.price = price;
        this.title = title;
        this.description = description;
        this.finished = 0;
    }

    public void setFinished(boolean finished) {
        this.finished = finished? 1:0;
    }

    public boolean getFinished() {
        return finished==1;
    }

    public Customer getBuyer() {
        return buyer;
    }

    public void setBuyer(Customer buyer) {
        this.buyer = buyer;
    }

    public Customer getSeller() {
        return seller;
    }

    public String getProductName() {
        return productName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price){this.price = price;}

    public void setSeller(Customer c) {
        this.seller = c;
    }

    public void setProductName(String p) {
        this.productName = p;
    }

    /*public void setPrice(Double price) {
        if (price > this.price)
            this.price = price;
        else
            System.out.println("for a small amount");
    } */

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @Override
    public String toString() {
        return "Auction{" +
                "buyer=" + buyer +
                ", seller=" + seller +
                ", product=" + productName +
                ", price=" + price +
                ", date=" + endDate.toString() +
                ", finished="+finished.toString()+
                '}';
    }
}
