package com.projekt.scheduled;

import com.projekt.dao.AuctionDAO;
import com.projekt.model.Auction;
import com.projekt.service.EmailNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.projekt.dao.imp.AuctionDAOimp;

import java.util.List;

@Component
public class AuctionStatusSchedule {

    private static final Logger logger = LogManager.getLogger(AuctionStatusSchedule.class);

    @Autowired
    EmailNotificationService emailNotificationService;

    @Scheduled(fixedDelay = 60*1000)
    public void auctionStatus(){
        logger.trace("Sprawdzanie statusu aukcji.");
        AuctionDAO dao = new AuctionDAOimp();
        List<Auction> auctions = dao.findFinished();
        logger.trace("Poprawianie statusu "+auctions.size() + "aucji.");
        for(Auction a : auctions){
            logger.trace("Aktualizacja: "+a.toString());
            emailNotificationService.sendNotification(a);
            a.setFinished(true);
            dao.update(a);
            logger.trace("Po aktualizacji: "+a.toString());
        }
        logger.info("Uaktualniono status aukcji");
    }
}
