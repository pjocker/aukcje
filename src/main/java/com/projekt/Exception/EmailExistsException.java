package com.projekt.Exception;

public class EmailExistsException extends Throwable {
    public EmailExistsException(String s) {
        super(s);
    }
}
