package com.projekt.service;

import com.projekt.model.Auction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
@Service
public class EmailNotificationService {
    private JavaMailSender javaMailSender;
    private static final Logger logger = LogManager.getLogger(EmailNotificationService.class);
    @Autowired
    public EmailNotificationService(JavaMailSender javaMailSender){
        this.javaMailSender = javaMailSender;
    }

    public void sendNotification(Auction auction) throws MailException {
        logger.info("Wysyłanie powiadomienia.");
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(auction.getBuyer().getEmail());
        mail.setFrom("javaaukcje@gmail.com");
        mail.setSubject("Zakończenie aukcji - " + auction.getTitle());
        mail.setText("Witaj, " + auction.getBuyer().getFirstName() + "!\n" +
                "Wygrałeś aukcję, w której brałeś udział.\n" +
                "Sprzedawca: " + auction.getSeller().getFirstName() + " " + auction.getSeller().getLastName() + "\n" +
                "Przedmiot aukcji: " + auction.getProductName() + "\n" +
                "Cena: " + auction.getPrice() + " PLN\n" +
                "Pozdrawiamy,\nAdministratorzy");
        logger.trace("Mail do kupującego: "+mail.toString());
        javaMailSender.send(mail);

        SimpleMailMessage mail2 = new SimpleMailMessage();
        mail2.setTo(auction.getSeller().getEmail());
        mail2.setFrom("javaaukcje@gmail.com");
        mail2.setSubject("Zakończenie aukcji - " + auction.getTitle());
        mail2.setText("Witaj, "+auction.getSeller().getFirstName()+"!\n"
                + "Użytkownik "+auction.getBuyer().getFirstName()+" "+auction.getBuyer().getLastName()
                + " wygrał prowadzoną przez ciebie aukcję.\n"
                + "Przedmiot: "+auction.getProductName()+"\n"
                + "Cena: "+auction.getPrice()+" PLN\n"
                + "Pozdrawiamy, \nAdministratorzy");
        logger.trace("Mail do sprzedającego: "+mail2.toString());
        javaMailSender.send(mail2);
    }
}
