package com.projekt.service;

import com.projekt.dao.AuctionDAO;
import com.projekt.dao.CustomerDAO;
import com.projekt.model.Auction;
import com.projekt.model.Customer;
import com.projekt.model.Login;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AuctionServiceImp implements AuctionService{
    @Autowired
    private AuctionDAO auctionDAO;
    @Autowired
    private CustomerDAO customerDAO;
    @Autowired
    private CustomerService customerService;
    private static final Logger logger = LogManager.getLogger(AuctionServiceImp.class);
    @Transactional
    public Auction createNewAuction(Auction auction, Customer customer){
        logger.info("Tworzenie nowej aukcji "+auction.getTitle());
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        date = c.getTime();
        auction.setSeller(customer);
        auction.setEndDate(date);
        auction.setFinished(false);
        logger.trace(auction.toString());
        logger.trace("Nowa aukcja: "+auction.toString());
        return auctionDAO.save(auction);
    }

    @Transactional
    public List<Auction> getAllAuctions(){
        return auctionDAO.findAll();
    }

    public Auction findById(Long auctionId){return auctionDAO.findbyid(auctionId);}

    @Transactional
    public Auction updateAuction(Auction auction){
        logger.info("Podbicie ceny aukcji "+auction.getTitle() + " o "+auction.getPrice()+ " przez "+ auction.getBuyer().getEmail());
        logger.trace(auction.toString());
        Auction usAuction = auctionDAO.findbyid(auction.getId());
        if(auction.getPrice() > 0) {
            usAuction.setPrice(usAuction.getPrice() + auction.getPrice());
            usAuction.setBuyer(auction.getBuyer());
            auctionDAO.update(usAuction);
        }
        return auctionDAO.findbyid(auction.getId());
    }

    @Override
    public List<Auction> getActiveAuctions() {
        return auctionDAO.findActive();
    }

    @Override
    public void deleteAuction(Login login, Long id){
        logger.info("Usuwanie aukcji, login: "+login.getEmail());
        Auction auction = auctionDAO.findbyid(id);
        Customer customer = customerService.findbyemail(login);
         System.out.print(login.getPassword()+" "+login.getEmail()+ " " + id);
         System.out.print(auction.getSeller().getId()+" "+customer.getId());
        logger.trace(login.getPassword()+" "+login.getEmail()+ " " + id);
        logger.trace(auction.getSeller().getId()+" "+customer.getId());
        if(customer!=null && auction!=null && auction.getSeller().getId() == customer.getId() && customer.getPassword().equals(login.getPassword())) {
            logger.trace("Delete id: " + id);
            auctionDAO.delete(id);
        }
        else
            System.out.print("nope");
    }

    public List<Auction> findCustomerAuctions(Login login){
        logger.info("Szukanie aukcji użytkownika "+login.getEmail());
        Customer customer = customerDAO.findbyemail(login);
        List<Auction> list;
        if(customer!= null)
            list = auctionDAO.findCustomerAuctions(customer);
        else list = new ArrayList<>();
        logger.trace("Znaleziono "+list.size()+" aukcji");
        for(Auction a : list){
            logger.trace(a.toString());
        }
        return list;
    }
}
