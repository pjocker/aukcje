package com.projekt.service;

import com.projekt.model.Auction;
import com.projekt.model.Customer;
import com.projekt.model.Login;

import java.util.List;


public interface AuctionService {
    Auction createNewAuction(Auction auction, Customer customer);
    List<Auction> getAllAuctions();
    Auction findById(Long auctionId);
    Auction updateAuction(Auction auction);
    List<Auction> getActiveAuctions();
    void deleteAuction(Login login, Long id);
    List<Auction> findCustomerAuctions(Login login);
}
