package com.projekt.service;

import com.projekt.Exception.EmailExistsException;
import com.projekt.model.Customer;
import com.projekt.model.Login;

import java.util.List;

public interface CustomerService {
    Customer registerNewUserAccount(Customer accountDto) throws EmailExistsException;
    Customer findbyemail(Login login);
    Customer save(Customer customer);
    Customer findbyid(Long id);
    List<Customer> findAll();
}