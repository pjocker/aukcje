package com.projekt.service;

import com.projekt.Exception.EmailExistsException;
import com.projekt.dao.CustomerDAO;
import com.projekt.model.Customer;
import com.projekt.model.Login;
import com.projekt.service.CustomerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CustommerServiceImp implements CustomerService {

    @Autowired
    private CustomerDAO customerDAO;

    public Customer findbyemail(Login login){
        return customerDAO.findbyemail(login);
    }

    @Override
    public Customer save(Customer customer) {
        return customerDAO.save(customer);
    }

    @Override
    public Customer findbyid(Long id) {
        return customerDAO.findbyid(id);
    }

    @Override
    public List<Customer> findAll() {
        return customerDAO.findAll();
    }

    private static final Logger logger = LogManager.getLogger(AuctionServiceImp.class);

    @Transactional
    @Override
    public Customer registerNewUserAccount(Customer account) throws EmailExistsException {
        /*if(emailExist(account.getEmail())){
            throw new EmailExistsException("Istnieje juz uzytkownik o emailu: " + account.getEmail());
        }*/
        logger.info("Rejestrowanie użytkownika "+ account.getFirstName() + " " + account.getLastName());
        final Customer customer = new Customer();
        customer.setFirstName(account.getFirstName());
        customer.setLastName(account.getLastName());
        customer.setEmail(account.getEmail());
        customer.setCity(account.getCity());
        customer.setAddress(account.getAddress());
        customer.setPhone(account.getPhone());
        customer.setPassword(account.getPassword());
        logger.trace(customer.toString());
        return customerDAO.save(customer);
    }

    /*private boolean emailExist(final String email) {
        Customer customer = customerDAO.findbyemail(email);
        if(customer != null){
            return true;
        }
        return false;
    }*/
}
