package com.projekt.dao;

import com.projekt.model.Product;


public interface ProductDAO extends GenericDAO<Product, Long>  {
}
