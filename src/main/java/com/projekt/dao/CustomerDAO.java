package com.projekt.dao;


import com.projekt.model.Customer;
import com.projekt.model.Login;

public interface CustomerDAO extends GenericDAO<Customer, Long> {

    Customer findbyemail(Login login);
}
