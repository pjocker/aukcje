package com.projekt.dao.imp;


import com.projekt.dao.CustomerDAO;
import com.projekt.model.Customer;
import com.projekt.model.Login;
import com.projekt.util.HibernateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDAOimp extends GenericDAOimp<Customer, Long> implements CustomerDAO {

    private final Class<Customer> type;
    private static final Logger logger = LogManager.getLogger(CustomerDAOimp.class);
    public CustomerDAOimp() {
        Type t =  getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType)t;
        type = (Class)pt.getActualTypeArguments()[0];
    }

    @Override
    public Customer findbyemail(Login login) {
        logger.info("Wykonywanie zapytania: Customer.findByEmail");
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query q = session.createQuery("from Customer where email in :email");
        q.setParameter("email",login.getEmail());
        List<Customer> customerList = q.list();
        logger.trace("Znaleziono "+customerList.size());
        for(Customer c : customerList){
            logger.trace(c.toString());
        }
        session.close();
        if (!customerList.isEmpty()) {
            return customerList.get(0);
        } else {
            return null;
        }
    }
}