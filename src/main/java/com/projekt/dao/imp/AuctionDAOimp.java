package com.projekt.dao.imp;


import com.projekt.dao.AuctionDAO;
import com.projekt.model.Auction;
import com.projekt.model.Customer;
import com.projekt.util.HibernateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
public class AuctionDAOimp extends GenericDAOimp<Auction, Long> implements AuctionDAO {
    private static final Logger logger = LogManager.getLogger(AuctionDAOimp.class);
    @Override
    public List<Auction> findFinished(){
                logger.info("Wykonywanie zapytania: Auction.findFinished");
                SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
                Session session = sessionFactory.openSession();
                Transaction transaction = session.beginTransaction();
                Query q = session.createQuery("from Auction where current_timestamp >= endDate and finished=0");
                List<Auction> result = q.list();
                logger.trace("Znaleziono: "+result.size());
                for(Auction a: result){
                    logger.trace(a.toString());
                }
                session.close();
                return result;
            }
    @Override
    public List<Auction> findActive(){
        logger.info("Wykonywanie zapytania: Auction.findActive");
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query q = session.createQuery("from Auction where finished=0");
        List<Auction> result = q.list();
        session.close();
        return result;
    }

    @Override
    public List<Auction> findCustomerAuctions(Customer customer){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query q = session.getNamedQuery("findSellerAuctions").setParameter("to", customer);

        List<Auction> result = q.list();
        session.close();
        return result;
    }

}
