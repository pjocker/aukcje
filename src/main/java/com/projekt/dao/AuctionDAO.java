package com.projekt.dao;


import com.projekt.model.Auction;
import com.projekt.model.Customer;

import java.util.Date;
import java.util.List;

public interface AuctionDAO extends GenericDAO<Auction, Long> {
    List<Auction> findFinished();
    List<Auction> findActive();
    List<Auction> findCustomerAuctions(Customer customer);
}
