package com.projekt.dao;


import java.util.List;

public interface GenericDAO<T, K> {
    T save(T t);
    void delete(K id);
    void update(T t);
    T findbyid(K id);
    List<T> findAll();
}
