package com.projekt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
    @RequestMapping(value = {"/", "/welcome**"})
    public String welcome(Model model){
        model.addAttribute("greeting","Witaj w Alledrogo");
        model.addAttribute("tagline","Skorzystaj z naszej oferty");
        return "welcome";
    }

    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage(){
        ModelAndView model = new ModelAndView();
        model.addObject("title","Witaj");
        model.addObject("message","Protected");
        model.setViewName("admin");

        return model;
    }

}
