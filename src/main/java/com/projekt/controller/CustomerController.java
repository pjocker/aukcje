package com.projekt.controller;

import com.projekt.Exception.EmailExistsException;
import com.projekt.model.Customer;
import com.projekt.model.Login;
import com.projekt.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping("/customer")
    public String list(Model model) {
        model.addAttribute("customer", customerService.findbyid((long) 1));
        return "customer";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegistrationForm(WebRequest request, Model model) {
        Customer customer = new Customer();
        model.addAttribute("customer", customer);
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registerUserAccount(
            @ModelAttribute("customer") Customer account,
            BindingResult result, HttpServletRequest request) {


            if(result.hasErrors()){
                return "registration";
            }

            customerService.save(account);

            return "redirect:/welcome";
    }

    private Customer createUserAccount(Customer account, BindingResult result) {
        Customer registered = null;
        try {
            registered = customerService.registerNewUserAccount(account);
        } catch (EmailExistsException e) {
            return null;
        }
        return registered;
    }

    /*@RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView init(ModelAndView model,
                       @RequestPart(value = "error", required = false) String error,
                       @RequestPart(value = "logout", required = false) String logout){
        if(error != null){
            model.addObject("error", "Twoj email lub haslo jest nie poprawne");
        }
        if(logout!=null){
            model.addObject("msg","Zostałes wylogowany");
        }
        model.setViewName("login");
        return model;
    }*/

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("login", new Login());
        return mav;
    }
    @RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
    public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
                                     @ModelAttribute("login") Login login) {
        ModelAndView mav = null;
        Customer user = customerService.findbyemail(login);
        if (user != null && login.getPassword().equals(user.getPassword())) {
            mav = new ModelAndView("admin");
            mav.addObject("firstname", user.getFirstName());
            mav.addObject("lastname", user.getLastName());
            mav.addObject("email", user.getEmail());
        } else {
            mav = new ModelAndView("login");

            mav.addObject("message", "Bledny email lub haslo!!");
        }
        return mav;
    }
}