package com.projekt.controller;

import com.projekt.model.Auction;
import com.projekt.model.Customer;
import com.projekt.model.Login;
import com.projekt.model.Product;
import com.projekt.service.AuctionService;
import com.projekt.service.CustomerService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.ejb.Init;
import javax.jws.WebParam;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

@Controller
@RequestMapping("/auctions")
public class AuctionController {

    @Autowired
    private AuctionService auctionService;

    @Autowired
    private CustomerService customerService;

    @RequestMapping
    public String list(Model model){
        model.addAttribute("auctions", auctionService.getActiveAuctions());
        return "auctions";
    }
    @RequestMapping(value="/add", method = RequestMethod.GET)
    public String getAddNewAuctionForm(Model model){
        Auction newAuction = new Auction();
        model.addAttribute("newAuction", newAuction);
        model.addAttribute("loginAuction", new Login());
        return "newAuction";
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String postAddNewAuctionForm(
            @ModelAttribute("newAuction") @Valid Auction newAuction,
            @ModelAttribute("loginAuction") Login login,
            BindingResult result){
        Customer customer = customerService.findbyemail(login);
        if(customer!=null && customer.getPassword().equals(login.getPassword())) {
            if (result.hasErrors()) {
                return "newAuction";
            } else {
                auctionService.createNewAuction(newAuction, customer);
                return "redirect:/auctions";
            }
        }else{
            return "welcome";
        }
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public String getRemoveUserAuctions(Model model, @ModelAttribute("loginRemove") final Login login){
        model.addAttribute("removeAuctions", auctionService.findCustomerAuctions(login));
        model.addAttribute("auction", new Auction());
        model.addAttribute("login", login);
        return "delete";
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String postRemoveUserAuctions(@ModelAttribute("auction") Auction auction, @ModelAttribute("login") Login login){
        auctionService.deleteAuction(login, auction.getId());
        return "redirect:/auctions";
    }

    @RequestMapping(value = "/remove/login", method = RequestMethod.GET)
    public ModelAndView getRemoveLogin(){
        ModelAndView mav = new ModelAndView("loginR");
        mav.addObject("loginRemove", new Login());
        return mav;
    }

    @RequestMapping(value = "/remove/login", method = RequestMethod.POST)
    public String postRemoveLogin(@ModelAttribute("loginRemove") final Login login, final RedirectAttributes redirectAttributes){
        Customer user = customerService.findbyemail(login);
        if(user != null && user.getPassword().equals(login.getPassword())){
            redirectAttributes.addFlashAttribute("loginRemove", login);
            return "redirect:/auctions/remove";
        }
        else {
           return "loginR";
        }
    }

    @RequestMapping("/{customerId}/remove")
    public String getProductsByCategory(Model model, @PathVariable("customerId") Long customerId){
        model.addAttribute("products", auctionService);
        return "products";
    }

    @InitBinder("newAuction")
    public void initialiseBinder(WebDataBinder binder){
        binder.setDisallowedFields("seller","buyer","endDate","finished");
    }

    @InitBinder("bidPrice")
    public void bigBinder(WebDataBinder binder){
        binder.setAllowedFields("price");
    }

    @InitBinder("auction")
    public void removeBinder(WebDataBinder binder){
        binder.setAllowedFields("id");
    }

    @RequestMapping(value = "/auction", method = RequestMethod.GET)
    public String getProductById(@RequestParam("id") Long auctionId, Model model){
        Auction auction = new Auction();
        model.addAttribute("auction", auctionService.findById(auctionId));
        model.addAttribute("bidPrice",auction);
        model.addAttribute("bidlogin",new Login());
        return "auction";
    }

    @RequestMapping(value = "/auction", method = RequestMethod.POST)
    public String bidProductPrice(
            @RequestParam("id") Long auctionId,
            @ModelAttribute("bidlogin")  Login login,
            @ModelAttribute("bidPrice")  Auction updateAuction){
        Customer customer = customerService.findbyemail(login);
        if(customer!=null && customer.getPassword().equals(login.getPassword())) {
            updateAuction.setId(auctionId);
            updateAuction.setBuyer(customer);
            auctionService.updateAuction(updateAuction);
            return "redirect:/auctions";
        }else{
            return "redirect:/auctions";
        }
    }



}
